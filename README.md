Software Freedom Day Festival (year 1394/jalali)
================================================

This repository is for contributing on proposal (copy-)writing of
Software Freedom Day Festival for year 1394 of jalali calendar.

Dependancies
------------

You must have:  
**Not Included:**

- [Nazli font](http://openfontlibrary.org/en/font/nazli#Nazli-Bold)
- A working version of ((xe)la)tex. (Recommanded: [TeX Live](https://www.tug.org/texlive/))

**Included:**

- B e s m e l l a h 1 font (assets/fonts/besmellah1.ttf)
- lfkf logo (assets/images/lfkf.png)
- licensing logo (assets/images/by.png)

Compilition
-----------

To compile `.tex` files five times do:

```bash
    $ xelatex proposal.tex 
```

Then the output is written to proposal.pdf  
***NOTE:*** Compiling five times is needed that in each times, `xelatex` creates some files that be used in later compiles.

Miscellaneous
-------------

***NOTE:*** Please rename `gitignore` to `.gitignore`
